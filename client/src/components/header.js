import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class Header extends Component {
    renderContent () {
        switch (this.props.auth) {
            case null:
                return;
            default:
                return <Menu.Item href="/home"><Button secondary>Load all Github Jobs</Button></Menu.Item>;
        }
    }

    render () {
        return (
            <Menu size='mini'>
                <Menu.Item name='home' as={ Link } to={this.props.auth ? '/surveys' : '/'}>
                    <i aria-hidden="true" className="home big icon"></i>
                </Menu.Item>
                <Menu.Menu position='right'>
                    {this.renderContent()}
                </Menu.Menu>
            </Menu>
        );
    }
}

function mapStateToProps ({auth}) {
    return { auth };
}

export default connect(mapStateToProps)(Header);
