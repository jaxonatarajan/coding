import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Segment, Container, Divider, Header, Image, Menu} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class JobDetailView extends Component {
    render () {
        const { gitHubAll, location } = this.props;
        console.log('loc' + location);

        if (!gitHubAll.length || !location) {
            return (<div>Loading...</div>);
        }

        const job = gitHubAll.find(j => j.id === location.query.id);

        return job ? (
            <div>
                <Menu size='mini'>
                    <Menu.Item position='right' name='home' as={ Link } to={'/home'}>
                        Back Home
                    </Menu.Item>
                </Menu>
                <Segment color='grey'>
                    <Image floated='right' size='small' src={job.company_logo}></Image>
                    <div floated='right'>
                        {job.created_at}
                    </div>
                    <Header as='h1'>{job.title}</Header>
                    <Header as='h3'>{job.location}</Header>
                    <Header as='h3'>{job.type}</Header>
                    <Container textAlign='justified'>
                        <Divider />
                        {job.description}
                    </Container>
                    <div className="content">
                        <div className="company">
                            <div className="name">{job.company}</div>
                            <div className="name"><span>Company URL: </span> <a target='_blank' href={job.company_url}>{job.company_url}</a></div>
                            <div className="name"><span>Job URL: </span><a target='_blank' href={job.url}>{job.url}</a></div>
                        </div>
                    </div>
                </Segment>
                <Menu size='mini'>
                    <Menu.Item position='right' name='home' as={ Link } to={'/home'}>
                        Back Home
                    </Menu.Item>
                </Menu>
            </div>
        ) : (
            <div>Error: job doesn't exist</div>
        );
    }
}

function mapStateToProps ({gitHubAll}) {
    console.log('yayayayaya' + {gitHubAll});
    return {gitHubAll};
}

export default connect(mapStateToProps)(JobDetailView);


