import React, { Component } from 'react';
import { BrowserRouter, Route} from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Header from './header';
import JobDetail from './jobDetail';
import GitHubJobs from './gitHubJobs';
import { Container } from 'semantic-ui-react';

class App extends Component {
    componentDidMount () {
        this.props.fetchGitHubJobs();
    }

    render () {
        return (
            <div>
                <BrowserRouter>
                    <Container>
                        <Header/>
                        <Route exact path="/" component={GitHubJobs}></Route>
                        <Route exact path="/home" component={GitHubJobs}></Route>
                        <Route exact path="/jobs/*" component={JobDetail}></Route>
                    </Container>
                </BrowserRouter>
            </div>
        );
    }
}
console.log(actions);
export default connect(null, actions)(App);
