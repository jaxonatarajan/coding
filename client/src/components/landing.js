import React from 'react';
import { Container, Message } from 'semantic-ui-react';

const Landing = () => {
    return (
        <Container textAlign='center'>
            <Message
                header='Welcome to the Github Job Board'
                content="We updated our job pages quite frequently, if you don't find the job you're after, come back later."
            />
        </Container>
    );
}

export default Landing;