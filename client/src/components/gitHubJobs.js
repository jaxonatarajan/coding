import React, { Component } from 'react';
import { connect } from 'react-redux';
import Truncate from 'react-truncate';
import { Button, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class GitHubJobs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: props.gitHubAll
        };
    }

    componentWillReceiveProps () {
        this.setState({
            items: this.state.gitHubAll
        });
    }

    renderContent () {
        return this.props.gitHubAll;
    }

    render () {
        console.log('success' + this.props.gitHubAll);
        if (this.props.gitHubAll.length > 1) {
            return (
                <div className="ui divided items">
                    {this.props.gitHubAll.map((job) =>
                        <div className="item" key = {job.id}>
                            <div className="image">
                                <img src={job.company_logo} />
                            </div>
                            <div className="content">
                                <a className="header">{job.title}</a>
                                <div className="meta">
                                    <span className="cinema">{job.location}</span>
                                </div>
                                <div className="type">
                                    {job.type}
                                </div>
                                <div className="extra">
                                </div>
                                <Truncate lines={3} ellipsis={<span>... <Link to={{pathname: `jobs/${job.id}`, query: { id: job.id }}}>
                                    <button className="primary ui right floated button" role="button">
                                        Full job description</button>
                                    </Link></span>}>
                                    {job.description}
                                </Truncate>
                            </div>
                        </div>
                    )}
                </div>
            )
        } else {
            return (
                <div>Data loading ..... </div>
            )
        }
    }
}

function mapStateToProps ({gitHubAll}) {
    console.log({gitHubAll});
    return {gitHubAll};
}

export default connect(mapStateToProps)(GitHubJobs);
