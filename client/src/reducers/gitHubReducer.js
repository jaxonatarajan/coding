import { FETCH_GITHUB } from '../actions/types';

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_GITHUB:
            return action.payload || false;
        default:
            return state;
    }

}
