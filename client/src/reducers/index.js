import { combineReducers } from 'redux';
import gitHubReducer from './gitHubReducer';

export default combineReducers({
    gitHubAll: gitHubReducer
});
