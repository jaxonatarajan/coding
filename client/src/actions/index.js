import axios from 'axios';
import { FETCH_GITHUB } from './types';

export const fetchGitHubJobs = () =>
     async dispatch => {
         const res = await axios.get('/api/gitHub/get');
         dispatch({ type: FETCH_GITHUB, payload: res.data});
    }

