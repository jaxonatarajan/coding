'use strict';
let jobDbModule = require('../../../database/modules/job');
let fields = 'id created_at title location type description how_to_apply company company_url company_logo url';

let jobs = {
    'getOne': (req, res, next) => {
        let id = req.params.id;
        let callback = (err, record) => {
            if (err) {
                next(err);
            }
            res.json(record);
        };

        jobDbModule.getOne(id, fields, callback);
    },
    'getAll': (req, res, next) => {
        let callback = (err, list) => {
            if (err) {
                next(err);
            }
            res.json(list);
        };

        jobDbModule.getAll(fields, callback);
    }
};

module.exports = jobs;
