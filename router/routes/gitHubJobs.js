'use strict';
var express = require('express');
var router = express.Router();
var job = require('./modules/job');

router.get('/get/:id',
    job.getOne
);

router.get('/get',
    job.getAll
);

module.exports = router;
