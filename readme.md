1. Checkout the master branch to your local machine and cd into the directory;
2. Do an npm install in the main server directory and as well as the client directory;
3. run 'npm run dev' from the terminal. This will run the server and client concurrently;
4. The above command should auto open a browser and load the default web application view, which is http://localhost:3000
5. The default view loads all the job profiles from a remove mongo db database;
6. Clicking on the 'full job description' button should show the job details view
7. There's a 'Back Home' button on this screen to navigate the user back to the home page
