'use strict';
let keys = require('../config/keys');

module.exports = () => {
    'use strict';
    // set undefined db url variable
    let dbUrl;

    // get database url from the config
    dbUrl = keys.mongoURI;

    // export db connection functions
    let db = require('./connect');

    // connect to the db
    db.connect(dbUrl, (err) => {
        if (err) {
            console.log('database index : Unable to connect to MongoDB');
        } else {
            console.log('database index : MongoDB connected successfully');
        }
    });
};
