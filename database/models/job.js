const mongoose = require('mongoose');
const { Schema } = mongoose;

const jobSchema = new Schema({
    id: {
        type: String,
        unique: false,
        required: false
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        unique: false,
        required: false
    },
    location: {
        type: String,
        unique: false,
        required: false
    },
    type: {
        type: String,
        unique: false,
        required: false
    },
    description: [{
        type: String,
        required: false,
        index: false
    }],
    how_to_apply: {
        type: String,
        unique: false,
        required: false
    },
    company: {
        type: String,
        unique: false,
        required: false
    },
    company_url: {
        type: String,
        unique: false,
        required: false
    },
    company_logo: {
        type: String,
        unique: false,
        required: false
    },
    url: {
        type: String,
        unique: false,
        required: false
    }
});

module.exports = mongoose.model('job', jobSchema, 'job');
