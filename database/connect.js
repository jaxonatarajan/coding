let mongoose = require('mongoose');
// prevents mongoose from using mpromise
mongoose.Promise = global.Promise;
//expose mongoose to be able to stub it in unit tests
exports.mongoose = mongoose;

let state = {
    db: null
};

exports.connect = (url, options, done) => {
    if (state.db) {
        return done();
    }
    mongoose.connect(url, options, (err) => {
        if (err) return done(err);
        state.db = mongoose.connection;
        done(null);
    });
};

exports.get = () => {
    return state.db;
};

exports.close = (done) => {
    if (state.db) {
        state.db.close((err, result) => {
            state.db = null;
            state.mode = null;
            done(err);
        });
    }
};
