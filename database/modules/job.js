module.exports = (function () {
    'use strict';

    let Job = require('../models/job');

    let getOne = (id, data, callback) => {
        Job.findOne({'_id': id}, data, (err, result) => {
            callback(err, result);
        });
    },
    getAll = (fields, callback) => {
        Job.find({}, fields, (err, list) => {
            callback(err, list);
        });
    };

    return {
        getOne: getOne,
        getAll: getAll
    };
})();
