const https = require('https');
const keys = require('../config/keys');

https.get(keys.positionURL, (res) => {
    res.on('data', (d) => {
        process.stdout.write(d);
        console.log(JSON.parse(d));
    });
}).on('error', (e) => {
    console.error(e);
});