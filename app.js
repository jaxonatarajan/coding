const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const keys = require('./config/keys');

mongoose.connect(keys.mongoURI);

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
 * DB connection
 */
let db = require('./database')();

/**
 * Routes
 */
require('./router')(app);
const PORT = process.env.PORT || 5000;
app.listen(PORT);
